import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

 

@Injectable()
export class UsersService {
  
 private _url = 'https://jsonplaceholder.typicode.com/users';


  getUsers()
 {
   return this._http.get(this._url).map(res => res.json()).delay(2000);   
 }
  constructor(private _http:Http) { }

}





/*   let users = [     זה היה שימוש בתוך גטיוזרס

  {name:'liad', email:'liad@liad.com'},
  {name:'rony', email:'rony@liad.com'},
  {name:'ammdo', email:'ammdo@liad.com'}

    ]
  return users;*/